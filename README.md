

Project to track all scripts that I use with OBS.

Note: ``poetry`` is not really used for the virtual env.
Using pyenv instead, until I can verify how to create env with shared libs with poetry ... 

```
CONFIGURE_OPTS=--enable-shared pyenv install 3.9.5
```

references:
- https://obsproject.com/forum/threads/zoom-and-follow.127743/post-531135
- https://github.com/tryptech/obs-zoom-and-follow/issues/20#issuecomment-859408073


